# RFC: TinyLogs format

Status: Draft
Last update: 2021-06-20

## Introduction

A Tinylog is a simple file with all "microblog" style entries to share small contents and interact with other other geminauts' tinylog.

The original idea and most "rules" comes from Drew/uoou/Friendo:

=> gemini://friendo.monster/log/lace.gmi

The goal of this RFC is to standardize the tinylog format for better coherence between authors by providing a consistent approach.
This will also allow better tools around tinylogs to improve both authors and readers experience reading micro entries.


## Tinylog format

This tinylog format proposal is composed of a mandatory part that tinylogs must be compatible with and optional information that author could add as meta information about their tinylog.


### Mandatory format

Each tinylog entry must be formated as follow:
```
## <Date>
<Content>
```

It starts with a second level header (`##`), followed by a space and then `<date>`.

`<Date>` format: The format extend the date format of the [gemini feed standard](https://gemini.circumlunar.space/docs/companion/subscription.gmi) (YYYY-MM-DD):

```
YYYY-MM-DD hh:mm TZ
```

`TZ` should either be:

* A [timezone abbreviations](https://en.wikipedia.org/wiki/List_of_time_zone_abbreviations) like UTC, CEST, ET, BST, …
* A valid UTC offset (eg: +02:00 for CEST, -06:00 for ET, …).

**Nota**: Timezone abbreviations are not standards (eg BST is an abbreviations of 3 different timezone). For better comprehension for any tools, the UTC offset is usually better.

If the timezone is not precised, UTC should be assumed.

Entries should be in order from newest to oldest.


`<Content>` format: Any text without 2 new lines.

Each entry must be separated with 2 new lines (ie paragraph break).

Example:
```
## 2021-06-20 20:40 +0200
A small thought to share.

## 2021-06-20 20:30 CEST
A first tinylog entry
```

### Optional information

#### Optional Header information

Additional informations could be added at the top of your tinylog file:
```
# <Tinylog Title>

<description>

author: <author>
avatar: <emoji>
licence: <licence>
```

`<Tinylog Title>` Can be any text without a line break.

`<description>`: Can be any text without 2 line breaks (= paragraph break). Description can be before or after the metadata but not between.

`<author>` format: `@authorName[@capsule.tld]` with authorName the tinylog author name and "capsule.tld" the url of the author capsule. The emoji and capsule url are optional. This intends to simplify communication between authors as well as discovery of new tinylog feeds.

Example:
```
author: @bacardi55
author: @bacardi55@gmi.bacardi55.io
```

`<emoji>` format: emoji [emoji description]. The description is optional.

Example:
```
avatar: 🦪
avatar: 🦪 (:oyster:, U+1F9AA)
```

`<licence>` format: the name of the licence.

Other additional metadata could be added but shouldn't be expected.

#### Optional response information

It's common in the gemini world to respond to a particular entry of an author. In that case, author should add a line below the header line with formatted `RE: @author[@capsule.tld] <Date of the original article>`

eg:
```
## 2021-06-20 22:30 CEST
Re: @user 2021-06-20 22:30 CEST
A response to @user: hello, cool post!

## 2021-06-20 22:30 CEST
Re: @user@capsule.tld 2021-06-20 22:30 CEST
A response to @user: hello, cool post!
```

It will be up to client to decide to use this information or not.

# Additional information

* [Tools around the tinylog format](Tools.md)
* [Unofficial list of tinylog](Known-tinylogs.md)
